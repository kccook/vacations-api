package com.casey.vacations;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@WebMvcTest(VacationController.class)
public class VacationControllerTests {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    VacationService vacationService;

    @Test
    public void getVacationsReturnsListOfVacations() throws Exception {
        Vacation vacation = new Vacation("Dells Trip", "WI", "Wisconsin Dells", 2021, 9);
        VacationList list = new VacationList(Arrays.asList(vacation));

        when(vacationService.getVacations()).thenReturn(list);

        mockMvc.perform(MockMvcRequestBuilders.get("/vacations"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.vacations", hasSize(1)));
    }
}
