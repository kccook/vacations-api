package com.casey.vacations;

import java.util.List;

public class VacationList {

    private List<Vacation> vacations;

    public VacationList() {}

    public VacationList(List<Vacation> vacations) {
        this.vacations = vacations;
    }

    public List<Vacation> getVacationList() {
        return vacations;
    }

    public void setVacationList(List<Vacation> vacationList) {
        this.vacations = vacations;
    }
}
