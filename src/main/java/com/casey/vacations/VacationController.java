package com.casey.vacations;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VacationController {

    VacationService vacationService;

    public VacationController(VacationService vacationService) {
        this.vacationService = vacationService;
    }

    @GetMapping("/vacations")
    public VacationList getAllVacations() {
            return vacationService.getVacations();
    }
}

